import { Site, SchemaType, Term } from '@hestia-earth/schema';
import { iso31662ToName } from '@hestia-earth/glossary';
import { reduceUndefinedValues } from '@hestia-earth/utils';

import { Location as OLCALocation } from '../olca';

const getCountry = (code = ''): Term => {
  const name = code.length >= 2 ? iso31662ToName(code.substring(0, 2)) : undefined;
  return name ? { type: SchemaType.Term, name } : undefined;
};

const getRegion = (code = ''): Term => {
  const name = code.length > 2 ? iso31662ToName(code) : undefined;
  return name ? { type: SchemaType.Term, name } : undefined;
};

export default (location: OLCALocation) => {
  const hasCoordinates = !!location.latitude || !!location.longitude;
  return [
    reduceUndefinedValues({
      type: SchemaType.Site,
      id: location['@id'],
      name: location.name,
      description: location.description,
      latitude: hasCoordinates ? location.latitude : undefined,
      longitude: hasCoordinates ? location.longitude : undefined,
      boundary: location.geometry,
      country: getCountry(location.code),
      region: getRegion(location.code),
      dataPrivate: false
    }) as Site
  ];
};
