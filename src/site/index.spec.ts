import { expect } from 'chai';
import 'mocha';
import { SchemaType } from '@hestia-earth/schema';

import { readFileAsJSON } from '../../test/utils';

import { OLCAType, Location } from '../olca';
import convert from './';

describe('site > index', () => {
  describe('convert', () => {
    it('should convert to hestia format', () => {
      expect([
        convert(readFileAsJSON('site/cf1fe73c-b3b8-3e6a-a362-a24cc00edca8.json'))
      ]).to.deep.equal([
        [
          {
            type: SchemaType.Site,
            id: 'cf1fe73c-b3b8-3e6a-a362-a24cc00edca8',
            name: 'US-NC',
            country: {
              type: SchemaType.Term,
              name: 'United States'
            },
            region: {
              type: SchemaType.Term,
              name: 'North Carolina'
            },
            dataPrivate: false
          }
        ]
      ]);
    });

    describe('latitude and longitude', () => {
      const location: Location = {
        '@context': 'context',
        '@type': OLCAType.Location,
        '@id': 'id'
      };

      describe('are both equal to 0', () => {
        beforeEach(() => {
          location.longitude = 0;
          location.latitude = 0;
        });

        it('should not set both', () => {
          expect(convert(location)).to.deep.equal([{
            type: 'Site',
            id: 'id',
            dataPrivate: false
          }]);
        });
      });

      describe('one of is equal to 0', () => {
        beforeEach(() => {
          location.longitude = 0;
          location.latitude = 10;
        });

        it('should set both', () => {
          expect(convert(location)).to.deep.equal([{
            type: 'Site',
            id: 'id',
            longitude: 0,
            latitude: 10,
            dataPrivate: false
          }]);
        });
      });
    });
  });
});
