import { OLCAType, Entity } from './olca';

export type entitiesByType = { [type in OLCAType]?: Entity[] };

export type convertFunction = (node: any, entities?: entitiesByType) => any;
