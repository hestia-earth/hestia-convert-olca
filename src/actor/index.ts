import { Actor, SchemaType } from '@hestia-earth/schema';
import { reduceUndefinedValues } from '@hestia-earth/utils';

import { Actor as OLCAActor } from '../olca';

const getActorNames = (name = '') => {
  const nameParts = name.split(' ');
  return nameParts.length === 2 ? {
    name: `${nameParts[0].substring(0, 1)} ${nameParts[1]}`,
    firstName: nameParts[0],
    lastName: nameParts[1]
  } : {};
};

export default (actor: OLCAActor) => {
  const names = getActorNames(actor.name);
  return [
    reduceUndefinedValues({
      type: SchemaType.Actor,
      id: actor['@id'],
      email: actor.email,
      website: (actor.website ? {
        '@id': actor.website
      } : undefined),
      country: (actor.country ? {
        type: SchemaType.Term,
        name: actor.country
      } : undefined),
      city: actor.city,
      dataPrivate: false,
      ...names
    }) as Actor
  ];
};
