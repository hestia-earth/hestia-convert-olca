import { expect } from 'chai';
import 'mocha';
import { SchemaType } from '@hestia-earth/schema';

import { readFileAsJSON } from '../../test/utils';

import convert from './';

describe('actor > index', () => {
  describe('convert', () => {
    it('should convert to hestia format', () => {
      expect([
        convert(readFileAsJSON('actor/24a9e595-cba9-308f-8191-78737c61fc1d.json')),
        convert(readFileAsJSON('actor/f10c4f80-d602-4315-a5da-8c36cd906fb3.json'))
      ]).to.deep.equal([
        [
          {
            type: SchemaType.Actor,
            id: '24a9e595-cba9-308f-8191-78737c61fc1d',
            name: 'J Cooper',
            firstName: 'Joyce',
            lastName: 'Cooper',
            email: 'cooperjs@u.washington.edu',
            website: {
              '@id': 'faculty.washington.edu/cooperjs'
            },
            dataPrivate: false
          }
        ],
        [
          {
            type: SchemaType.Actor,
            id: 'f10c4f80-d602-4315-a5da-8c36cd906fb3',
            country: {
              type: SchemaType.Term,
              name: 'USA'
            },
            dataPrivate: false
          }
        ]
      ]);
    });
  });
});
