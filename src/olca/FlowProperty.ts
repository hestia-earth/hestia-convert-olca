// auto-generated content
import { FlowPropertyType } from './FlowPropertyType';
import { UnitGroup } from './UnitGroup';
import { CategorizedEntity } from './CategorizedEntity';

/**
 * A flow property is a quantity that can be used to express amounts of a flow.
 */
export class FlowProperty extends CategorizedEntity {
  /**
   * The type of the flow property
   */
  flowPropertyType?: FlowPropertyType;

  /**
   * The units of measure that can be used to express quantities of the flow property.
   */
  unitGroup?: UnitGroup;

}
