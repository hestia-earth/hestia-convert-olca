// auto-generated content
import { Currency } from './Currency';
import { FlowRef } from './FlowRef';
import { FlowProperty } from './FlowProperty';
import { ProcessRef } from './ProcessRef';
import { Unit } from './Unit';
import { Uncertainty } from './Uncertainty';
import { Entity } from './Entity';

/**
 * An Exchange is an input or output of a [Flow] in a [Process]. The amount of an exchange is given in a specific unit
 *  of a quantity ([FlowProperty]) of the flow. The allowed units and flow properties that can be used for a flow in a
 * n exchange are defined by the flow property information in that flow (see also the [FlowPropertyFactor] type).
 */
export class Exchange extends Entity {
  /**
   * Indicates whether this exchange is an avoided product.
   */
  avoidedProduct?: boolean;

  /**
   * A formula for calculating the costs of this exchange.
   */
  costFormula?: string;

  /**
   * The costs of this exchange.
   */
  costValue?: number;

  /**
   * The currency in which the costs of this exchange are given.
   */
  currency?: Currency;

  /**
   * The process internal ID of the exchange. This is used to identify exchanges unambiguously within a process (e.g. wh
   * en linking exchanges in a product system where multiple exchanges with the same flow are allowed). The value should
   *  be >= 1.
   */
  internalId?: number;

  /**
   * The reference to the flow of the exchange.
   */
  flow?: FlowRef;

  /**
   * The quantity in which the amount is given.
   */
  flowProperty?: FlowProperty;

  input?: boolean;

  /**
   * Indicates whether the exchange is the quantitative reference of the process.
   */
  quantitativeReference?: boolean;

  baseUncertainty?: number;

  /**
   * A default provider is a [Process] that is linked as the provider of a product input or the waste treatment provider
   *  of a waste output. It is just an optional default setting which can be also ignored when building product systems
   * in openLCA. The user is always free to link processes in product systems ignoring these defaults (but the flows and
   *  flow directions have to match of course).
   */
  defaultProvider?: ProcessRef;

  amount?: number;

  amountFormula?: string;

  unit?: Unit;

  /**
   * A data quality entry like `(1;3;2;5;1)`. The entry is a vector of data quality values that need to match the data q
   * uality scheme for flow inputs and outputs that is assigned to the [Process]. In such a scheme the data quality indi
   * cators have fixed positions and the respective values in the `dqEntry` vector map to these positions.
   */
  dqEntry?: string;

  uncertainty?: Uncertainty;

  /**
   * A general comment about the input or output.
   */
  description?: string;

}
