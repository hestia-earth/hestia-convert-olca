// auto-generated content
import { Ref } from './Ref';
import { FlowMapEntry } from './FlowMapEntry';
import { RootEntity } from './RootEntity';

/**
 * A crosswalk of flows from a source flow list to a target flow list.
 */
export class FlowMap extends RootEntity {
  /**
   * The reference (id, name, description) of the source flow list.
   */
  source?: Ref;

  /**
   * The reference (id, name, description) of the target flow list.
   */
  target?: Ref;

  /**
   * A list of flow mappings from flows in a source flow list to flows in a target flow list.
   */
  mappings?: FlowMapEntry[];

}
