// auto-generated content
import { Entity } from './Entity';

/**
 * An score value of an indicator ([DqIndicator]) in a data quality system ([DqSystem]).
 */
export class DqScore extends Entity {

  position?: number;

  label?: string;

  description?: string;

  uncertainty?: number;

}
