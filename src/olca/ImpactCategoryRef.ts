// auto-generated content
import { Ref } from './Ref';

/**
 * A reference to a [ImpactCategory] data set.
 */
export class ImpactCategoryRef extends Ref {
  /**
   * The name (symbol) of the reference unit of the impact category.
   */
  refUnit?: string;

}
