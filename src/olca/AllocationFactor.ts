// auto-generated content
import { AllocationType } from './AllocationType';
import { FlowRef } from './FlowRef';
import { ExchangeRef } from './ExchangeRef';
import { Entity } from './Entity';

/**
 * A single allocation factor in a process.
 */
export class AllocationFactor extends Entity {
  /**
   * The type of allocation.
   */
  allocationType?: AllocationType;

  /**
   * The output product (or waste input) to which this allocation factor is related. The must be an exchange with this p
   * roduct output (or waste input) in this process.
   */
  product?: FlowRef;

  /**
   * The value of the allocation factor.
   */
  value?: number;

  /**
   * An optional formula from which the value of the allocation factor is calculated.
   */
  formula?: string;

  /**
   * A product input, waste output, or elementary flow exchange which is allocated by this factor. This is only valid fo
   * r causal allocation where allocation factors can be assigned to single exchanges.
   */
  exchange?: ExchangeRef;

}
