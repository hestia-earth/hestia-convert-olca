// auto-generated content
import { FlowRef } from './FlowRef';
import { Location } from './Location';
import { FlowProperty } from './FlowProperty';
import { Unit } from './Unit';
import { Uncertainty } from './Uncertainty';
import { Entity } from './Entity';

/**
 * A single characterisation factor of a LCIA category for a flow.
 */
export class ImpactFactor extends Entity {
  /**
   * The [Flow] of the impact assessment factor.
   */
  flow?: FlowRef;

  /**
   * In case of a regionalized impact category, this field can contain the location for which this factor is valid.
   */
  location?: Location;

  /**
   * The quantity of the flow to which the LCIA factor is related (e.g. Mass).
   */
  flowProperty?: FlowProperty;

  /**
   * The flow unit to which the LCIA factor is related (e.g. kg).
   */
  unit?: Unit;

  /**
   * The value of the impact assessment factor.
   */
  value?: number;

  /**
   * A mathematical formula for calculating the value of the LCIA factor.
   */
  formula?: string;

  /**
   * The uncertainty distribution of the factors' value.
   */
  uncertainty?: Uncertainty;

}
