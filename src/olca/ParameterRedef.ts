// auto-generated content
import { Ref } from './Ref';
import { Entity } from './Entity';

/**
 * A redefinition of a parameter in a product system.
 */
export class ParameterRedef extends Entity {
  /**
   * The parameter name.
   */
  name?: string;

  /**
   * The (new) value of the parameter.
   */
  value?: number;

  /**
   * The context of the paramater (a process or LCIA method). If no context is provided it is assumed that this is a red
   * efinition of a global parameter.
   */
  context?: Ref;

}
