// auto-generated content
import { Process } from './Process';
import { Flow } from './Flow';
import { Exchange } from './Exchange';
import { Entity } from './Entity';

/**
 * A process link is a connection between two processes in a product system.
 */
export class ProcessLink extends Entity {
  /**
   * The descriptor of the process that provides a product or a waste treatment.
   */
  provider?: Process;

  /**
   * The descriptor of the flow that is exchanged between the two processes.
   */
  flow?: Flow;

  /**
   * The descriptor of the process that is linked to the provider.
   */
  process?: Process;

  /**
   * The exchange of the linked process (this is useful if the linked process has multiple exchanges with the same flow
   * that are linked to different provides, e.g. in an electricity mix).
   */
  exchange?: Exchange;

}
