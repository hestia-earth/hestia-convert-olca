// auto-generated content
import { FlowMapRef } from './FlowMapRef';
import { RootEntity } from './RootEntity';

/**
 * A mapping from one flow to another.
 */
export class FlowMapEntry extends RootEntity {
  /**
   * The flow, flow property, and unit of the source flow.
   */
  from?: FlowMapRef;

  /**
   * The flow, flow property, and unit of the target flow.
   */
  to?: FlowMapRef;

  /**
   * The factor to convert the original source flow to the target flow.
   */
  conversionFactor?: number;

}
