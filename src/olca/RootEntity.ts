// auto-generated content
import { Entity } from './Entity';

/**
 * A standalone item in a database like a location, unit group, flow, or process. A root entity can be unambiguously i
 * dentified by its id (the JSON-LD @id field), version, and lastChange fields.
 */
export class RootEntity extends Entity {
  /**
   * The name of the entity.
   */
  name?: string;

  /**
   * The description of the entity.
   */
  description?: string;

  /**
   * A version number in MAJOR.MINOR.PATCH format where the MINOR and PATCH fields are optional and the fields may have
   * leading zeros (so 01.00.00 is the same as 1.0.0 or 1).
   */
  version?: string;

  /**
   * The timestamp when the entity was changed the last time.
   */
  lastChange?: Date;

}
