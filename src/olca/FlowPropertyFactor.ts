// auto-generated content
import { FlowProperty } from './FlowProperty';
import { Entity } from './Entity';

/**
 * A FlowPropertyFactor is a conversion factor between <a href="./FlowProperty.html">flow properties (quantities)</a>
 * of a <a href="./Flow.html">flow</a>. As an example the amount of the flow 'water' in a process could be expressed i
 * n 'kg' mass or 'm3' volume. In this case the flow water would have two flow property factors: one for the flow prop
 * erty 'mass' and one for 'volume'. Each of these flow properties has a reference to a <a href="./UnitGroup.html">uni
 * t group</a> which again has a reference unit. In the example the flow property 'mass' could reference the unit grou
 * p 'units of mass' with 'kg' as reference unit and volume could reference the unit group 'units of volume' with 'm3'
 *  as reference unit. The flow property factor is now the conversion factor between these two reference units where t
 * he factor of the reference flow property of the flow is 1. If the reference flow property of 'water' in the example
 *  would be 'mass' the respective flow property factor would be 1 and the factor for 'volume' would be 0.001 (as 1 kg
 *  water is 0.001 m3). The amount of water in a process can now be also given in liter, tons, grams etc. For this, th
 * e unit conversion factor of the respective unit group can be used to convert into the reference unit (which then ca
 * n be used to convert to the reference unit of another flow property). Another thing to note is that different flow
 * properties can refer to the same unit group (e.g. MJ upper calorific value and MJ lower calorific value.)
 */
export class FlowPropertyFactor extends Entity {
  /**
   * The flow property (quantity) of the factor.
   */
  flowProperty?: FlowProperty;

  /**
   * The value of the conversion factor.
   */
  conversionFactor?: number;

  /**
   * Indicates whether the flow property of the factor is the reference flow property of the flow. The reference flow pr
   * operty must have a conversion factor of 1.0 and there should be only one reference flow property.
   */
  referenceFlowProperty?: boolean;

}
