// auto-generated content
import { RiskLevel } from './RiskLevel';
import { SocialIndicator } from './SocialIndicator';
import { Source } from './Source';
import { Entity } from './Entity';

/**
 * An instance of this class describes a social aspect related to a social indicator in a process.
 */
export class SocialAspect extends Entity {
  /**
   * The value of the activity variable of the related indicator.
   */
  activityValue?: number;

  comment?: string;

  /**
   * A data quality entry, e.g. `(3,1,2,4,1)`.
   */
  quality?: string;

  /**
   * The raw amount of the indicator's unit of measurement (not required to be numeric currently)
   */
  rawAmount?: string;

  riskLevel?: RiskLevel;

  socialIndicator?: SocialIndicator;

  source?: Source;

}
