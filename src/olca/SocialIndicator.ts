// auto-generated content
import { FlowProperty } from './FlowProperty';
import { Unit } from './Unit';
import { CategorizedEntity } from './CategorizedEntity';

export class SocialIndicator extends CategorizedEntity {
  /**
   * The name of the activity variable of the indicator.
   */
  activityVariable?: string;

  /**
   * The quantity of the activity variable.
   */
  activityQuantity?: FlowProperty;

  /**
   * The unit of the activity variable.
   */
  activityUnit?: Unit;

  /**
   * The unit in which the indicator is measured.
   */
  unitOfMeasurement?: string;

  /**
   * Documentation of the evaluation scheme of the indicator.
   */
  evaluationScheme?: string;

}
