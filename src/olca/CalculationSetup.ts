// auto-generated content
import { CalculationType } from './CalculationType';
import { ProductSystem } from './ProductSystem';
import { ImpactMethod } from './ImpactMethod';
import { NwSet } from './NwSet';
import { AllocationType } from './AllocationType';
import { ParameterRedef } from './ParameterRedef';
import { Unit } from './Unit';
import { FlowProperty } from './FlowProperty';
import { Entity } from './Entity';

/**
 * A setup for a product system calculation.
 */
export class CalculationSetup extends Entity {
  /**
   * The type of calculation that should be performed.
   */
  calculationType?: CalculationType;

  /**
   * The product system that should be calculated (required).
   */
  productSystem?: ProductSystem;

  /**
   * The LCIA method for the calculation (optional).
   */
  impactMethod?: ImpactMethod;

  /**
   * Indicates whether life cycle costs should be also calculated (optional).
   */
  withCosts?: boolean;

  /**
   * The normalisation and weighting set for the calculation (optional).
   */
  nwSet?: NwSet;

  /**
   * The calculation type to be used in the calculation (optional).
   */
  allocationMethod?: AllocationType;

  /**
   * A list of parameter redefinitions to be used in the calculation (optional).
   */
  parameterRedefs?: ParameterRedef[];

  /**
   * (optional)
   */
  amount?: number;

  /**
   * (optional)
   */
  unit?: Unit;

  /**
   * (optional)
   */
  flowProperty?: FlowProperty;

}
