// auto-generated content
import { NwFactor } from './NwFactor';
import { RootEntity } from './RootEntity';

/**
 * A normalization and weighting set.
 */
export class NwSet extends RootEntity {
  /**
   * This is the optional unit of the (normalized and) weighted score when this normalization and weighting set was appl
   * ied on a LCIA result.
   */
  weightedScoreUnit?: string;

  /**
   * The list of normalization and weighting factors of this set.
   */
  factors?: NwFactor[];

}
