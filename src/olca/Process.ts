// auto-generated content
import { AllocationFactor } from './AllocationFactor';
import { AllocationType } from './AllocationType';
import { Exchange } from './Exchange';
import { Location } from './Location';
import { Parameter } from './Parameter';
import { ProcessDocumentation } from './ProcessDocumentation';
import { ProcessType } from './ProcessType';
import { DqSystem } from './DqSystem';
import { SocialAspect } from './SocialAspect';
import { CategorizedEntity } from './CategorizedEntity';

export class Process extends CategorizedEntity {

  allocationFactors?: AllocationFactor[];

  defaultAllocationMethod?: AllocationType;

  /**
   * The inputs and outputs of the process.
   */
  exchanges?: Exchange[];

  /**
   * This field holds the last internal ID that was used in an exchange (which may have been deleted, so it can be large
   * r than the largest internal ID of the exchanges of the process.) The internal ID of an exchange is used to identify
   *  exchanges within a process (for updates, data exchanges (see process links), etc.). When you add an exchange to a
   * process, you should increment this field in the process and set the resulting value as the internal ID of that exch
   * ange. The sequence of internal IDs should start with `1`.
   */
  lastInternalId?: number;

  location?: Location;

  parameters?: Parameter[];

  processDocumentation?: ProcessDocumentation;

  processType?: ProcessType;

  /**
   * A reference to a data quality system ([DqSystem]) with which the overall quality of the process can be assessed.
   */
  dqSystem?: DqSystem;

  /**
   * A reference to a data quality system ([DqSystem]) with which the quality of individual inputs and outputs ([Exchang
   * e]s) of the process can be assessed.
   */
  exchangeDqSystem?: DqSystem;

  /**
   * A reference to a data quality system ([DqSystem]) with which the quality of individual social aspects of the proces
   * s can be assessed.
   */
  socialDqSystem?: DqSystem;

  /**
   * A data quality entry like `(1;3;2;5;1)`. The entry is a vector of data quality values that need to match the overal
   * l data quality system of the process (the system that is stored in the `dqSystem` property). In such a system the d
   * ata quality indicators have fixed positions and the respective values in the `dqEntry` vector map to these position
   * s.
   */
  dqEntry?: string;

  /**
   * Indicates whether this process describes an infrastructure process. This field is part of the openLCA schema becaus
   * e of backward compatibility with EcoSpold 1. It does not really have a meaning in openLCA and should not be used an
   * ymore.
   */
  infrastructureProcess?: boolean;

  /**
   * A set of social aspects related to this process.
   */
  socialAspects?: SocialAspect[];

}
