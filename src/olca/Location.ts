// auto-generated content
import { CategorizedEntity } from './CategorizedEntity';

/**
 * A location like a country, state, city, etc.
 */
export class Location extends CategorizedEntity {
  /**
   * The code of the location (e.g. an ISO 2-letter country code).
   */
  code?: string;

  /**
   * The average latitude of the location.
   */
  latitude?: number;

  /**
   * The average longitude of the location.
   */
  longitude?: number;

  /**
   * A GeoJSON object.
   */
  geometry?: any;

}
