// auto-generated content
import { ImpactCategoryRef } from './ImpactCategoryRef';
import { NwSet } from './NwSet';
import { CategorizedEntity } from './CategorizedEntity';

/**
 * An impact assessment method.
 */
export class ImpactMethod extends CategorizedEntity {
  /**
   * The impact categories of the method.
   */
  impactCategories?: ImpactCategoryRef[];

  /**
   * The normalization and weighting sets of the method.
   */
  nwSets?: NwSet[];

}
