// auto-generated content
import { OLCAType } from './types';

export interface IContext {
  '@base': string;
  '@vocab': string;
}

export class Entity {
  '@context': string|(string|IContext)[];
  '@type': OLCAType;
  '@id': string;
}
