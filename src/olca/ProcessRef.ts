// auto-generated content
import { ProcessType } from './ProcessType';
import { Ref } from './Ref';

/**
 * A reference to a [Process] data set.
 */
export class ProcessRef extends Ref {
  /**
   * The location name or code of the process.
   */
  location?: string;

  /**
   * The type of the process.
   */
  processType?: ProcessType;

}
