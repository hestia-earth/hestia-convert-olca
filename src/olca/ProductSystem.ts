// auto-generated content
import { ProcessRef } from './ProcessRef';
import { Exchange } from './Exchange';
import { Unit } from './Unit';
import { FlowProperty } from './FlowProperty';
import { ProcessLink } from './ProcessLink';
import { CategorizedEntity } from './CategorizedEntity';

/**
 * A product system describes the supply chain of a product (the functional unit) ...
 */
export class ProductSystem extends CategorizedEntity {
  /**
   * The descriptors of all processes that are contained in the product system.
   */
  processes?: ProcessRef[];

  /**
   * The descriptor of the process that provides the flow of the functional unit of the product system.
   */
  referenceProcess?: ProcessRef;

  /**
   * The exchange of the reference processes (typically the product output) that provides the flow of the functional uni
   * t of the product system.
   */
  referenceExchange?: Exchange;

  /**
   * The flow amount of the functional unit of the product system.
   */
  targetAmount?: number;

  /**
   * The unit in which the flow amount of the functional unit is given.
   */
  targetUnit?: Unit;

  /**
   * The flow property in which the flow amount of the functional unit is given.
   */
  targetFlowProperty?: FlowProperty;

  /**
   * The process links of the product system.
   */
  processLinks?: ProcessLink[];

}
