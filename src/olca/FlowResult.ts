// auto-generated content
import { FlowRef } from './FlowRef';
import { Entity } from './Entity';

/**
 * A result value for a flow; given in the reference unit of the flow.
 */
export class FlowResult extends Entity {
  /**
   * The flow reference.
   */
  flow?: FlowRef;

  /**
   * Indicates whether the flow is an input or not.
   */
  input?: boolean;

  /**
   * The value of the flow amount.
   */
  value?: number;

}
