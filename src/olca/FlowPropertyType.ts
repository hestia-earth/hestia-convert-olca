// auto-generated content

export enum FlowPropertyType {
  ECONOMIC_QUANTITY = 'ECONOMIC_QUANTITY',
  PHYSICAL_QUANTITY = 'PHYSICAL_QUANTITY'
}
