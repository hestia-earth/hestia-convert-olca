// auto-generated content
import { Entity } from './Entity';

/**
 * An instance of this class describes a reference to an exchange in a process. When we reference such an exchange we
 * only need the information to indentify that exchange unambiguously in a process.
 */
export class ExchangeRef extends Entity {
  /**
   * The internal ID of the exchange.
   */
  internalId?: number;

}
