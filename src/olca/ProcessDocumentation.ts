// auto-generated content
import { Actor } from './Actor';
import { Source } from './Source';
import { Entity } from './Entity';

export class ProcessDocumentation extends Entity {

  timeDescription?: string;

  validUntil?: Date;

  validFrom?: Date;

  technologyDescription?: string;

  dataCollectionDescription?: string;

  completenessDescription?: string;

  dataSelectionDescription?: string;

  reviewDetails?: string;

  dataTreatmentDescription?: string;

  inventoryMethodDescription?: string;

  modelingConstantsDescription?: string;

  reviewer?: Actor;

  samplingDescription?: string;

  sources?: Source[];

  restrictionsDescription?: string;

  copyright?: boolean;

  creationDate?: Date;

  dataDocumentor?: Actor;

  dataGenerator?: Actor;

  dataSetOwner?: Actor;

  intendedApplication?: string;

  projectDescription?: string;

  publication?: Source;

  geographyDescription?: string;

}
