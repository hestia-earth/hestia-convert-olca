// auto-generated content
import { Parameter } from './Parameter';
import { ImpactFactor } from './ImpactFactor';
import { CategorizedEntity } from './CategorizedEntity';

export class ImpactCategory extends CategorizedEntity {
  /**
   * The name of the reference unit of the LCIA category (e.g. kg CO2-eq.).
   */
  referenceUnitName?: string;

  /**
   * A set of parameters which can be used in formulas of the characterisation factors in this impact category.
   */
  parameters?: Parameter[];

  /**
   * The characterisation factors of the LCIA category.
   */
  impactFactors?: ImpactFactor[];

}
