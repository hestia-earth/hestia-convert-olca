// auto-generated content
import { FlowResult } from './FlowResult';
import { ImpactResult } from './ImpactResult';
import { Entity } from './Entity';

export class SimpleResult extends Entity {

  flowResults?: FlowResult[];

  impactResults?: ImpactResult[];

}
