// auto-generated content
import { CategorizedEntity } from './CategorizedEntity';

export class Currency extends CategorizedEntity {

  code?: string;

  conversionFactor?: number;

  referenceCurrency?: Currency;

}
