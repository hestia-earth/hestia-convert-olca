// auto-generated content
import { ImpactCategory } from './ImpactCategory';
import { Entity } from './Entity';

/**
 * A normalization and weighting factor of a [NwSet] related to an impact category. Depending on the purpose of the [N
 * wSet] (normalization, weighting, or both) the normalization and weighting factor can be present or not.
 */
export class NwFactor extends Entity {

  impactCategory?: ImpactCategory;

  normalisationFactor?: number;

  weightingFactor?: number;

}
