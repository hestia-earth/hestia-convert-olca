// auto-generated content
import { RootEntity } from './RootEntity';

/**
 * An unit of measure
 */
export class Unit extends RootEntity {
  /**
   * The conversion factor to the reference unit of the unit group to which this unit belongs.
   */
  conversionFactor?: number;

  /**
   * Indicates whether the unit is the reference unit of the unit group to which this unit belongs. If it is the referen
   * ce unit the conversion factor must be 1.0. There should be always only one reference unit in a unit group. The refe
   * rence unit is used to convert amounts given in one unit to amounts given in another unit of the respective unit gro
   * up.
   */
  referenceUnit?: boolean;

  /**
   * A list of synonyms for the unit.
   */
  synonyms?: string[];

}
