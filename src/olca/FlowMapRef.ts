// auto-generated content
import { FlowRef } from './FlowRef';
import { FlowProperty } from './FlowProperty';
import { Unit } from './Unit';
import { Entity } from './Entity';

/**
 * Describes a the source or target flow of a flow mapping in a `FlowMap`. Such a flow reference can also optionally s
 * pecify the unit and flow property (quantity) for which the mapping is valid. If the unit and quantity are not given
 * , the mapping is based on the reference unit of the reference flow property of the respective flow.
 */
export class FlowMapRef extends Entity {
  /**
   * The reference to the flow data set.
   */
  flow?: FlowRef;

  /**
   * An optional reference to a flow property of the flow for which the mapping is valid.
   */
  flowProperty?: FlowProperty;

  /**
   * An optional reference to a unit of the flow for which the mapping is valid
   */
  unit?: Unit;

}
