// auto-generated content
import { RootEntity } from './RootEntity';

/**
 * A Ref is a reference to a [RootEntity]. When serializing an entity (e.g. a [Process]) that references another stand
 * alone entity (e.g. a [Flow] in an [Exchange]) we do not want to write the complete referenced entity into the seria
 * lized JSON object but just a reference. However, the reference contains some meta-data like name, category path etc
 * . that are useful to display.
 */
export class Ref extends RootEntity {
  /**
   * The full path of the category of the referenced entity from top to bottom, e.g. `"Elementary flows", "Emissions to
   * air", "unspecified"`.
   */
  categoryPath?: string[];

}
