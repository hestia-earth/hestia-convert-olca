// auto-generated content
import { Category } from './Category';
import { RootEntity } from './RootEntity';

/**
 * A root entity which can have a category.
 */
export class CategorizedEntity extends RootEntity {
  /**
   * The category of the entity.
   */
  category?: Category;

  /**
   * A list of optional tags. A tag is just a string which should not contain commas (and other special characters).
   */
  tags?: string[];

  /**
   * If this entity is part of a library, this field contains the identifier of that library. The identifier is typicall
   * y just the comination of the library name and version.
   */
  library?: string;

}
