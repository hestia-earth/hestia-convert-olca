// auto-generated content
import { FlowType } from './FlowType';
import { FlowPropertyFactor } from './FlowPropertyFactor';
import { Location } from './Location';
import { CategorizedEntity } from './CategorizedEntity';

/**
 * Everything that can be an input or output of a process (e.g. a substance, a product, a waste, a service etc.)
 */
export class Flow extends CategorizedEntity {
  /**
   * The type of the flow. Note that this type is more a descriptor of how the flow is handled in calculations.
   */
  flowType?: FlowType;

  /**
   * A CAS number of the flow.
   */
  cas?: string;

  /**
   * A chemical formula of the flow.
   */
  formula?: string;

  /**
   * The flow properties (quantities) in which amounts of the flow can be expressed together with conversion factors bet
   * ween these flow flow properties.
   */
  flowProperties?: FlowPropertyFactor[];

  /**
   * The location of the flow. Normally the location of a flow is defined by the process location where the flow is an i
   * nput or output. However, some data formats define a location as a property of a flow.
   */
  location?: Location;

  /**
   * A list of synonyms but packed into a single field. Best is to use semicolons as separator as commas are sometimes u
   * sed in names of chemicals.
   */
  synonyms?: string;

  /**
   * Indicates whether this flow describes an infrastructure product. This field is part of the openLCA schema because o
   * f backward compatibility with EcoSpold 1. It does not really have a meaning in openLCA and should not be used anymo
   * re.
   */
  infrastructureFlow?: boolean;

}
