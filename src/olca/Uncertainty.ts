// auto-generated content
import { UncertaintyType } from './UncertaintyType';
import { Entity } from './Entity';

/**
 * Defines the parameter values of an uncertainty distribution. Depending on the uncertainty distribution type differe
 * nt parameters could be used.
 */
export class Uncertainty extends Entity {
  /**
   * The uncertainty distribution type
   */
  distributionType?: UncertaintyType;

  /**
   * The arithmetic mean (used for normal distributions).
   */
  mean?: number;

  /**
   * A mathematical formula for the arithmetic mean.
   */
  meanFormula?: string;

  /**
   * The geometric mean value (used for log-normal distributions).
   */
  geomMean?: number;

  /**
   * A mathematical formula for the geometric mean.
   */
  geomMeanFormula?: string;

  /**
   * The minimum value (used for uniform and triangle distributions).
   */
  minimum?: number;

  /**
   * A mathematical formula for the minimum value.
   */
  minimumFormula?: string;

  /**
   * The arithmetic standard deviation  (used for normal distributions).
   */
  sd?: number;

  /**
   * A mathematical formula for the arithmetic standard deviation.
   */
  sdFormula?: string;

  /**
   * The geometric standard deviation (used for log-normal distributions).
   */
  geomSd?: number;

  /**
   * A mathematical formula for the geometric standard deviation.
   */
  geomSdFormula?: string;

  /**
   * The most likely value (used for triangle distributions).
   */
  mode?: number;

  /**
   * A mathematical formula for the most likely value.
   */
  modeFormula?: string;

  /**
   * The maximum value (used for uniform and triangle distributions).
   */
  maximum?: number;

  /**
   * A mathematical formula for the maximum value.
   */
  maximumFormula?: string;

}
