// auto-generated content

export enum FlowType {
  ELEMENTARY_FLOW = 'ELEMENTARY_FLOW',
  PRODUCT_FLOW = 'PRODUCT_FLOW',
  WASTE_FLOW = 'WASTE_FLOW'
}
