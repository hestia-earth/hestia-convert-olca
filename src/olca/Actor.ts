// auto-generated content
import { CategorizedEntity } from './CategorizedEntity';

/**
 * An actor is a person or organisation.
 */
export class Actor extends CategorizedEntity {

  address?: string;

  city?: string;

  country?: string;

  email?: string;

  telefax?: string;

  telephone?: string;

  website?: string;

  zipCode?: string;

}
