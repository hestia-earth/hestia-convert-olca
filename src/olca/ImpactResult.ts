// auto-generated content
import { ImpactCategoryRef } from './ImpactCategoryRef';
import { Entity } from './Entity';

/**
 * A result value for an impact assessment category.
 */
export class ImpactResult extends Entity {
  /**
   * The reference to the impact assessment category.
   */
  impactCategory?: ImpactCategoryRef;

  /**
   * The value of the flow amount.
   */
  value?: number;

}
