// auto-generated content
import { ImpactMethod } from './ImpactMethod';
import { NwSet } from './NwSet';
import { CategorizedEntity } from './CategorizedEntity';

export class Project extends CategorizedEntity {

  impactMethod?: ImpactMethod;

  nwSet?: NwSet;

}
