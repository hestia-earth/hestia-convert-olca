// auto-generated content
import { CategorizedEntity } from './CategorizedEntity';

/**
 * A source is a literature reference.
 */
export class Source extends CategorizedEntity {
  /**
   * A URL that points to the source.
   */
  url?: string;

  /**
   * The full text reference of the source.
   */
  textReference?: string;

  /**
   * The publication year of the source.
   */
  year?: number;

  /**
   * A direct link (relative or absolute URL) to the source file.
   */
  externalFile?: string;

}
