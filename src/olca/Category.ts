// auto-generated content
import { ModelType } from './ModelType';
import { CategorizedEntity } from './CategorizedEntity';

/**
 * A category is used for the categorisation of types like processes, flows, etc. The tricky thing is that the `Catego
 * ry` class inherits also from the [CategorizedEntity] type so that a category can have a category attribute which is
 *  then the parent category of this category (uff).
 */
export class Category extends CategorizedEntity {
  /**
   * The type of models that can be linked to the category.
   */
  modelType?: ModelType;

}
