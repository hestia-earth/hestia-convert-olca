// auto-generated content
import { DqScore } from './DqScore';
import { Entity } from './Entity';

/**
 * An indicator of a data quality system ([DqSystem]).
 */
export class DqIndicator extends Entity {

  name?: string;

  position?: number;

  scores?: DqScore[];

}
