// auto-generated content
import { FlowProperty } from './FlowProperty';
import { Unit } from './Unit';
import { CategorizedEntity } from './CategorizedEntity';

/**
 * A group of units that can be converted into each other.
 */
export class UnitGroup extends CategorizedEntity {
  /**
   * Some LCA data formats do not have the concept of flow properties or quantities. This field provides a default link
   * to a flow property for units that are contained in this group.
   */
  defaultFlowProperty?: FlowProperty;

  /**
   * The units of the unit group.
   */
  units?: Unit[];

}
