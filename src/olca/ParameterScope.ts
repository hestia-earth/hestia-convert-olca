// auto-generated content

export enum ParameterScope {
  PROCESS_SCOPE = 'PROCESS_SCOPE',
  IMPACT_SCOPE = 'IMPACT_SCOPE',
  GLOBAL_SCOPE = 'GLOBAL_SCOPE'
}
