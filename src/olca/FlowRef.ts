// auto-generated content
import { FlowType } from './FlowType';
import { Ref } from './Ref';

/**
 * A reference to a [Flow] data set.
 */
export class FlowRef extends Ref {
  /**
   * The name (symbol) of the reference unit of the flow.
   */
  refUnit?: string;

  /**
   * The location name or code of the flow. Typically, this is only used for product flows in databases like ecoinvent.
   */
  location?: string;

  /**
   * The type of the flow.
   */
  flowType?: FlowType;

}
