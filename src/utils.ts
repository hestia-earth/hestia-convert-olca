import { JSON as HestiaJson, SchemaType } from '@hestia-earth/schema';

import { Entity } from './olca';

export const diffInDays = (from: string | Date, to: string | Date) =>
  Math.floor((new Date(to).getTime() - new Date(from).getTime()) / (1000 * 60 * 60 * 24));

export const findById = <T extends Entity>(values: T[], id: string) => (values || []).find(v => v['@id'] === id);

export const linkedNode = (node: any = {}): any =>
  (
    'type' in node && 'id' in node ? { type: node.type, id: node.id } : (
      '@type' in node && '@id' in node ? { '@type': node['@type'], '@id': node['@id'] } : undefined
    )
  );

const ordersOfNode = [
  SchemaType.Term,
  SchemaType.Organisation,
  SchemaType.Actor,
  SchemaType.Bibliography,
  SchemaType.Source,
  SchemaType.Site,
  SchemaType.Cycle,
  SchemaType.ImpactAssessment
];

export const sortNodes = (nodes: HestiaJson<SchemaType>[]) =>
  nodes.sort((node1, node2) =>
    node1.type === node2.type ?
      Object.keys(node2).length - Object.keys(node1).length : // take the one with more data on it
      ordersOfNode.indexOf(node1.type) - ordersOfNode.indexOf(node2.type)
  );
