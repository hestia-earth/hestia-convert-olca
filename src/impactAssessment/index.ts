import { ImpactAssessment, SchemaType, Cycle } from '@hestia-earth/schema';
import { reduceUndefinedValues } from '@hestia-earth/utils';

import { entitiesByType } from '../convert-interfaces';
import { ProductSystem, Process } from '../olca';
import convertCycle from '../cycle';

export default (product: ProductSystem, entities: entitiesByType = {}) => {
  const process: Process = (entities.Process || [])
    .find(p => product.referenceProcess && p['@id'] === product.referenceProcess['@id']);
  const cycle = process ?
    convertCycle(process, entities).find(({ type }) => type === SchemaType.Cycle) as Cycle :
    undefined;
  return [
    reduceUndefinedValues({
      type: SchemaType.ImpactAssessment,
      id: product['@id'],
      name: product.name,
      functionalUnitQuantity: product.targetAmount,
      versionDetails: product.version,
      ...(process ? {
        timestamp: process.lastChange,
        cycle
      } : {}),
      dataPrivate: false
    }) as ImpactAssessment
  ];
};
