import { expect } from 'chai';
import 'mocha';
import { SchemaType } from '@hestia-earth/schema';

import { readFileAsJSON } from '../../test/utils';

import convert from '.';

describe('impactAssessment > index', () => {
  describe('convert', () => {
    it('should convert to hestia format', () => {
      expect([
        convert(readFileAsJSON('inventory/a2d69fe3-1c1e-4137-9da8-44ea67a8cca3.json'))
      ]).to.deep.equal([
        [
          {
            type: SchemaType.ImpactAssessment,
            id: 'a2d69fe3-1c1e-4137-9da8-44ea67a8cca3',
            name: 'soybeans; at harvest; at farm; wet basis',
            functionalUnitQuantity: 1.0,
            versionDetails: '00.00.000',
            dataPrivate: false
          }
        ]
      ]);
    });
  });
});
