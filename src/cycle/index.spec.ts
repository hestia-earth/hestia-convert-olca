/* eslint-disable max-len */
import { expect } from 'chai';
import 'mocha';
import { SchemaType } from '@hestia-earth/schema';

import { readFileAsJSON } from '../../test/utils';

import convert from './';

describe('cycle > index', () => {
  describe('convert', () => {
    it('should convert to hestia format', () => {
      expect([
        convert(readFileAsJSON('cycle/6f324ea6-e19d-3bad-999e-4b8448f26753.json')),
        convert(readFileAsJSON('cycle/746c5f2d-bf1c-d6dd-8c34-2391eed8217c.json'))
      ]).to.deep.equal([
        [
          {
            type: SchemaType.Source,
            id: '18710428-2bb5-3060-b19a-423af71b84b8',
            intendedApplication: 'These data were developed as a part of a crop production product system, for use in "Accounting" and "Micro-level decision support," situations C2 and A as described in European Commission - Joint Research Centre - Institute for Environment and Sustainablility: International Reference Life Cycle Data System (ILCD) Handbook - General guide for Life Cycle Assessment - Detailed Guidance. First edition March 2010. EUR 24708 EN. Luxembourg. Publications Office of the European Union; 2010.'
          },
          {
            type: SchemaType.Cycle,
            id: '6f324ea6-e19d-3bad-999e-4b8448f26753',
            name: 'operation of Soybean grain cart; for harvesting soybeans; at farm; 190HP tractor, 1000 bu',
            description: 'This unit process/ gate-to-gate dataset represents the use of a operation of soybean grain cart for 1 pass over 1 hectare (1 ha) for harvesting soybeans produced in North Carolina (US State).',
            defaultSource: {
              type: SchemaType.Source,
              id: '18710428-2bb5-3060-b19a-423af71b84b8'
            },
            startDate: '2014-01-01',
            endDate: '2014-12-31',
            cycleDuration: 364,
            defaultMethodClassification: 'modelled',
            defaultMethodClassificationDescription: 'default',
            completeness: {
              type: 'Completeness',
              fertiliser: true,
              soilAmendments: true,
              excreta: true,
              water: true,
              electricityFuel: true,
              cropResidue: true,
              pesticidesAntibiotics: true,
              products: true,
              material: true,
              other: true,
              transport: true,
              waste: true,
              liveAnimals: true,
              animalFeed: true,
              operations: true
            },
            functionalUnitDetails: '1 ha',
            functionalUnit: '1 ha',
            inputs: [
              {
                type: SchemaType.Input,
                value: [
                  26.7835718792
                ],
                term: {
                  type: 'Term',
                  name: 'work; ag. tractors for soybeans harvest, 2014 fleet, all fuels; 175-300HP',
                  openLCAId: '07840bdb-7fa4-3cb2-b090-b4b8d3d0afb6'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  2.1569350661
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF storage time; farm equipment; at farm',
                  openLCAId: 'f5623af3-7a61-3a61-b57d-f11d5f18609e'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.0000210039
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF equipment retirement; soybean grain cart; at farm',
                  openLCAId: '4b77f7ac-2be4-315a-816a-0aeb5efe7fb3'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.0000210039
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF maintenance; soybean grain cart; at farm',
                  openLCAId: '1584d6db-7311-3371-8228-3b90c5402b75'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.0000210039
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF production; soybean grain cart; at farm',
                  openLCAId: '0fb7d65e-beed-33e6-9536-c76694674ba4'
                }
              }
            ],
            products: [
              {
                type: SchemaType.Product,
                value: [
                  1
                ],
                term: {
                  type: 'Term',
                  name: 'operation of Soybean grain cart; for harvesting soybeans; at farm; 190HP tractor, 1000 bu',
                  openLCAId: '0a25b2ce-f7a6-320d-b224-e9f6bc8f7e18'
                }
              }
            ],
            dataPrivate: false
          }
        ],
        [
          {
            type: SchemaType.Source,
            id: '93bb04bf-65cd-9ec2-f1cc-ccf3f1ad6ca8',
            intendedApplication: 'These data were developed as a part of a crop production product system, for use in "Accounting" and "Micro-level decision support," situations C2 and A as described in European Commission - Joint Research Centre - Institute for Environment and Sustainablility: International Reference Life Cycle Data System (ILCD) Handbook - General guide for Life Cycle Assessment - Detailed Guidance. First edition March 2010. EUR 24708 EN. Luxembourg. Publications Office of the European Union; 2010.'
          },
          {
            type: SchemaType.Cycle,
            id: '746c5f2d-bf1c-d6dd-8c34-2391eed8217c',
            name: 'work; ag. tractors for growing soybeans, 2014 fleet, all fuels; 50-100HP',
            description: "\nThis unit process/ gate-to-gate dataset represents the production of 1 MJ of work by 50-100HP ag. tractors operating in 2014 in North Carolina (US State).  Multiple fuels are used (Gasoline, Diesel).  The process technologies represented are an aggregation of the exhaust technologies and control technologies over multiple model years as described in the USEPA's NONROAD model documentation (available at http://www.epa.gov/otaq/nonrdmdl.htm). Applicable classifications include ISIC 0161, NAIC 115110, UNSPSC 25101901. This dataset was prepared in ILCD format using the UWDFE Lab Excel to ILCD File Generator (gamma version, March 2014).\n",
            defaultSource: {
              type: SchemaType.Source,
              id: '93bb04bf-65cd-9ec2-f1cc-ccf3f1ad6ca8'
            },
            startDate: '2014-01-01',
            endDate: '2014-12-31',
            cycleDuration: 364,
            defaultMethodClassification: 'modelled',
            defaultMethodClassificationDescription: 'default',
            completeness: {
              type: 'Completeness',
              fertiliser: false,
              soilAmendments: false,
              excreta: false,
              water: false,
              electricityFuel: false,
              cropResidue: false,
              pesticidesAntibiotics: false,
              products: false,
              material: false,
              other: false,
              transport: false,
              waste: false,
              liveAnimals: false,
              animalFeed: false,
              operations: false
            },
            functionalUnitDetails: '1 MJ',
            functionalUnit: 'relative',
            inputs: [
              {
                type: SchemaType.Input,
                value: [
                  0.0000304106466135563
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF lubricating oil management; at farm',
                  openLCAId: '521a9dab-8ec8-f266-f0b7-c82e9fb85bfe'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.00000150828303345993
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF spare part set; ag. tractors; 50-100HP',
                  openLCAId: '31a10402-a385-97e6-b978-7b96970a54c4'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.00000150828303345993
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF production; ag. tractors; 50-100HP',
                  openLCAId: 'babaecfa-6504-985c-c797-aaa1c311b53c'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.000171092443695238
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF gasoline; for offroad use; at refinery; RVP8, S%0.034, EtOH m%0.93b%0.104',
                  openLCAId: '6bc7589a-7938-8291-8ce9-d1c1096f78a4'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.0664662952150587
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF diesel; for offroad use; at refinery, S%0.002',
                  openLCAId: '644232e9-f932-1c55-7269-58bdc5eaad64'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.00000150828303345993
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF equ. retirement; ag. tractors; 50-100HP',
                  openLCAId: 'd7dfa7b4-215a-06a3-45d3-6e471fa2aa49'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.0049364582393698
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF local area trucking; class 6, average fuel',
                  openLCAId: '69567abd-c3fe-5f24-8378-6cf05c386fd8'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.0000173856808066626
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF storage; of farm equipment; at farm',
                  openLCAId: 'bf1fc9c6-ec9f-be53-4478-9c2f6c14cf6d'
                }
              },
              {
                type: SchemaType.Input,
                value: [
                  0.000152053233067782
                ],
                term: {
                  type: 'Term',
                  name: 'CUTOFF lubricating oil; for use in agricultural equipment; at refinery',
                  openLCAId: '85981847-6749-7490-ce21-e063247f02a2'
                }
              }
            ],
            products: [
              {
                type: SchemaType.Product,
                value: [
                  1
                ],
                term: {
                  type: 'Term',
                  name: 'work; ag. tractors for growing soybeans, 2014 fleet, all fuels; 50-100HP',
                  openLCAId: 'f25cd1d0-25e2-3f65-b4e7-fb32c2336b22'
                }
              }
            ],
            emissions: [
              {
                type: SchemaType.Emission,
                value: [
                  2.01687659694198e-11
                ],
                term: {
                  type: 'Term',
                  name: '2,3,7,8-Tetrachlorodibenzofuran',
                  openLCAId: '5ed841be-f0ef-39b6-d2c3-fd67ae62bbc8'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.29692557528529e-9
                ],
                term: {
                  type: 'Term',
                  name: 'Mercury (divalent gaseous)',
                  openLCAId: 'e4edf70e-251e-def6-d5c7-b1bdcfefa4f5'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.16977152765184e-10
                ],
                term: {
                  type: 'Term',
                  name: 'Anthracene',
                  openLCAId: 'df9d4dca-655b-460f-83b3-ee98efb35036'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00104459553759596
                ],
                term: {
                  type: 'Term',
                  name: 'Carbon monoxide, fossil',
                  openLCAId: '099b36ab-4c03-4587-87f4-2f81e337afb8'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.6861065052481e-8
                ],
                term: {
                  type: 'Term',
                  name: 'Fluorene',
                  openLCAId: '8c524507-938a-4ab9-8430-105b2011dbae'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  3.24120112089984e-10
                ],
                term: {
                  type: 'Term',
                  name: 'Chrysene',
                  openLCAId: '621d5583-dca8-4acc-add7-316f8fca9ebc'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00000188837833264055
                ],
                term: {
                  type: 'Term',
                  name: 'Propanal',
                  openLCAId: '963be8ca-e43d-4f8d-a97b-d8982035ffbc'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.000160970348851377
                ],
                term: {
                  type: 'Term',
                  name: 'NMVOC, non-methane volatile organic compounds, unspecified origin',
                  openLCAId: '33b38ccb-593b-4b11-b965-10d747ba3556'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  3.42697107359764e-11
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,7,8,9-Hexachlorodibenzo-p-Dioxin',
                  openLCAId: '8c76d00e-910a-6b0f-de69-0d1b13732a9e'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  4.86662024172969e-13
                ],
                term: {
                  type: 'Term',
                  name: 'Dibenzo(a,h)anthracene',
                  openLCAId: '966cf136-e54a-552f-6a71-aad3cf7b5909'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  2.59066092918879e-9
                ],
                term: {
                  type: 'Term',
                  name: 'Mercury',
                  openLCAId: '5ec9c16a-959d-44cd-be7d-a935727d2151'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.3456541349623e-7
                ],
                term: {
                  type: 'Term',
                  name: '2,2,4-Trimethylpentane',
                  openLCAId: '716e3af1-547d-f9bf-d840-f6dfc3660afa'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00000379754712780448
                ],
                term: {
                  type: 'Term',
                  name: 'Secondary Organic Aerosol',
                  openLCAId: '5e9cb499-a5ea-94af-e292-34652d5c19f1'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  3.05268923947224e-7
                ],
                term: {
                  type: 'Term',
                  name: 'Butadiene',
                  openLCAId: '8a970585-5ce2-4226-b60d-df1c34649a56'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  6.76104981407897e-12
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,7,8-Pentachlorodibenzo-p-Dioxin',
                  openLCAId: 'd668bf42-fd37-3a11-9252-9b9b48d00c59'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  8.1978476840108e-8
                ],
                term: {
                  type: 'Term',
                  name: 'Naphthalene',
                  openLCAId: '160cc5f4-4d25-43c3-9662-a62221505ec1'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.37091540001844e-11
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,6,7,8-Hexachlorodibenzofuran',
                  openLCAId: '66ab92a1-a3b4-b45a-e987-74e71dc1d286'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  6.24415915631722e-9
                ],
                term: {
                  type: 'Term',
                  name: 'Chromium VI',
                  openLCAId: '03b279a7-4626-4703-a451-d0650599dbd9'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00000329411232917161
                ],
                term: {
                  type: 'Term',
                  name: 'Benzene',
                  openLCAId: '5e883a00-04e6-4d96-8dce-12d7117c6635'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  5.52042003911134e-10
                ],
                term: {
                  type: 'Term',
                  name: 'Pyrene',
                  openLCAId: '14ede4f0-be68-4933-b84b-b4dc90f73fff'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.24420867687973e-10
                ],
                term: {
                  type: 'Term',
                  name: 'Benz(a)anthracene',
                  openLCAId: '8016cfd8-c95b-d175-f8ab-55c303e13068'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  7.97321923186352e-12
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,7,8,9-Hexachlorodibenzofuran',
                  openLCAId: '09abeb04-1599-47a4-37b9-5aa75dc40c0e'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.0000304840918941867
                ],
                term: {
                  type: 'Term',
                  name: 'Carbon dioxide, biogenic',
                  openLCAId: 'd6235194-e4e6-4548-bfa3-ac095131aef4'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00000245062448805531
                ],
                term: {
                  type: 'Term',
                  name: 'Toluene',
                  openLCAId: '77f17646-cede-4a49-99dd-55950098b077'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  2.90082681760156e-9
                ],
                term: {
                  type: 'Term',
                  name: 'Fluoranthene',
                  openLCAId: 'd1548388-ce45-4a78-ac65-e096c1da1f8e'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.000163645884749563
                ],
                term: {
                  type: 'Term',
                  name: 'Total Organic Gases',
                  openLCAId: 'adc5dd9c-d1e1-fab5-39db-ff1128752591'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.0000026187537835943
                ],
                term: {
                  type: 'Term',
                  name: 'Sulfur dioxide',
                  openLCAId: '78c3efe4-421c-4d30-82e4-b97ac5124993'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00000174447323876792
                ],
                term: {
                  type: 'Term',
                  name: 'Xylene',
                  openLCAId: 'a1d277aa-7aea-4b5e-8aa9-2789ba8ff333'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00173018300765799
                ],
                term: {
                  type: 'Term',
                  name: 'Ammonia',
                  openLCAId: '0f440cc0-0f74-446d-99d6-8ff0e97a2444'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.21210245670907e-8
                ],
                term: {
                  type: 'Term',
                  name: 'Chromium, ion',
                  openLCAId: 'd43572af-d023-443b-9fad-d56ffc0adc53'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  5.11303576272862e-7
                ],
                term: {
                  type: 'Term',
                  name: 'Benzene, ethyl-',
                  openLCAId: 'e0e605c8-370b-4b5a-b6d4-2e7a5a1a6709'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  7.10584922164858e-11
                ],
                term: {
                  type: 'Term',
                  name: 'Octachlorodibenzofuran',
                  openLCAId: 'c3bf8b34-f3da-70a9-97ae-0359facc2149'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.78118616551512e-11
                ],
                term: {
                  type: 'Term',
                  name: '2,3,4,7,8-Pentachlorodibenzofuran',
                  openLCAId: '10263be9-fd27-a9c6-11e3-9c37f0a275f6'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00000849436268394865
                ],
                term: {
                  type: 'Term',
                  name: 'Acetaldehyde',
                  openLCAId: 'fc9f9a81-c4b9-4dce-bc20-9f04e05ca2f1'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  6.40076508940869e-11
                ],
                term: {
                  type: 'Term',
                  name: 'Benzo(a)pyrene',
                  openLCAId: '81c4ba39-8a3f-4a43-97b4-401605bbebf5'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  9.51192285721284e-12
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,4,7,8-Hexachlorodibenzo-p-Dioxin',
                  openLCAId: '3e489c84-63e9-ae6d-1c7a-050fd36424b9'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  7.99720344352447e-12
                ],
                term: {
                  type: 'Term',
                  name: 'Dioxins, measured as 2,3,7,8-tetrachlorodibenzo-p-dioxin',
                  openLCAId: 'f77c5e36-ee47-4437-b757-03139bb1d6d6'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00000251513691646824
                ],
                term: {
                  type: 'Term',
                  name: 'Methane, fossil',
                  openLCAId: '70ef743b-3ed5-4a6d-b192-fb6d62378555'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  6.22597936271245e-11
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,4,6,7,8-Heptachlorodibenzofuran',
                  openLCAId: 'c5d33ced-3c33-606d-b265-f1e2f7959f40'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00160396609283802
                ],
                term: {
                  type: 'Term',
                  name: 'Nitrogen oxides',
                  openLCAId: '77357947-ccc5-438e-9996-95e65e1e1bce'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  8.08783452652172e-7
                ],
                term: {
                  type: 'Term',
                  name: 'Nickel',
                  openLCAId: 'e8fc62ba-678e-4706-97d2-b79d83e227d5'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  8.04447677243369e-12
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,7,8-Pentachlorodibenzofuran',
                  openLCAId: '7999fa72-f08c-bef4-4a15-70ce2fc1e9df'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.74754080053499e-11
                ],
                term: {
                  type: 'Term',
                  name: 'Indeno(1,2,3,c,d)pyrene',
                  openLCAId: '9e2d403e-c4f9-0684-5e76-850a4ded8210'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00000503865279603274
                ],
                term: {
                  type: 'Term',
                  name: 'Particulates, > 2.5 um, and < 10um',
                  openLCAId: '295c9740-6fdb-4676-9eb8-15e3786f713d'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  4.37656911913561e-8
                ],
                term: {
                  type: 'Term',
                  name: 'Phenanthrene',
                  openLCAId: 'c7b704e6-0ad7-458a-bdd5-edc4c91dd631'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  6.52443862410927e-9
                ],
                term: {
                  type: 'Term',
                  name: 'Arsenic & compounds',
                  openLCAId: 'e292ceba-bc59-3365-fa27-12fdd3885874'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.68199389361941e-8
                ],
                term: {
                  type: 'Term',
                  name: 'Acenaphthene',
                  openLCAId: '922a7ccf-3338-43ad-bd7e-77d51df051ab'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  6.6441920105252e-10
                ],
                term: {
                  type: 'Term',
                  name: 'Mercury (particulate)',
                  openLCAId: 'f6fc222a-e5ad-d750-d4e5-7aeec662ff31'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.43136475071658e-8
                ],
                term: {
                  type: 'Term',
                  name: 'Acenaphthylene',
                  openLCAId: 'ae1791b5-85ce-481a-9091-75c4b3f51d2c'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  4.85332225784557e-7
                ],
                term: {
                  type: 'Term',
                  name: 'Acrolein',
                  openLCAId: '12840cef-c3b8-4ef2-b7d4-3a73dbd6a280'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.0000189064275683923
                ],
                term: {
                  type: 'Term',
                  name: 'Formaldehyde',
                  openLCAId: '9167dca7-615e-435c-8ba6-dbbf50e50e34'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.00000534315527134574
                ],
                term: {
                  type: 'Term',
                  name: 'Dinitrogen monoxide',
                  openLCAId: 'afd6d670-bbb0-4625-9730-04088a5b035e'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.000161130747833095
                ],
                term: {
                  type: 'Term',
                  name: 'Non-Methane Organic Gases',
                  openLCAId: '1e95101a-d96f-cfa1-da1a-82a662e49203'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.86508722912469e-11
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,6,7,8-Hexachlorodibenzo-p-Dioxin',
                  openLCAId: '37223b2f-2a32-01bb-81a9-eec81d3f4044'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  2.23805787192584e-10
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,4,6,7,8-Heptachlorodibenzo-p-Dioxin',
                  openLCAId: 'f115da0a-f2e5-b5bc-a0b7-0e8dd1651c11'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.033962829173e-9
                ],
                term: {
                  type: 'Term',
                  name: 'Octachlorodibenzo-p-dioxin',
                  openLCAId: '411bd4fd-a7ae-907a-e077-689b7606ae2a'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  8.85561954494932e-11
                ],
                term: {
                  type: 'Term',
                  name: 'Benzo(b)fluoranthene',
                  openLCAId: '4480e7c7-93ce-b49f-36a9-799b631daf55'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  6.50621666963153e-11
                ],
                term: {
                  type: 'Term',
                  name: 'Benzo(k)fluoranthene',
                  openLCAId: 'e59c311b-ded2-4673-900a-c9e4a37fb9d7'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  1.98638526525129e-11
                ],
                term: {
                  type: 'Term',
                  name: '2,3,4,6,7,8-Hexachlorodibenzofuran',
                  openLCAId: 'ddaa836b-861d-f4e8-9454-499fbe78ae6b'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.000162828564088206
                ],
                term: {
                  type: 'Term',
                  name: 'Particulates, < 2.5 um',
                  openLCAId: '66f50b33-fd62-4fdd-a373-c5b0de7de00d'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  2.64851103483753e-7
                ],
                term: {
                  type: 'Term',
                  name: 'Hexane',
                  openLCAId: '41067ac7-bf36-47ef-a19f-683d9b758a28'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  3.00742868384778e-11
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,4,7,8-Hexachlorodibenzofuran',
                  openLCAId: 'dd8e67f7-4774-fbd3-78e3-7788034db940'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  7.08744500276121e-12
                ],
                term: {
                  type: 'Term',
                  name: '1,2,3,4,7,8,9-Heptachlorodibenzofuran',
                  openLCAId: '3b249c89-6168-d6d6-546b-22856e7838f0'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  9.56111424200853e-8
                ],
                term: {
                  type: 'Term',
                  name: 'Styrene',
                  openLCAId: '2d456fb8-592b-46a7-97f0-81fc2a18e587'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.000150463971474435
                ],
                term: {
                  type: 'Term',
                  name: 'Non-Methane Hydrocarbons',
                  openLCAId: 'e0efc2e4-46f7-bf77-832f-0a34a756c51c'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  4.55934587368542e-11
                ],
                term: {
                  type: 'Term',
                  name: 'Benzo(g,h,i)perylene',
                  openLCAId: '4150cf78-ef2b-2045-896d-0f5ac9529b95'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  0.210331547971521
                ],
                term: {
                  type: 'Term',
                  name: 'Carbon dioxide, fossil',
                  openLCAId: 'aa7cac3a-3625-41d4-bc54-33e2cf11ec46'
                }
              },
              {
                type: SchemaType.Emission,
                value: [
                  5.43473422826594e-7
                ],
                term: {
                  type: 'Term',
                  name: 'Manganese',
                  openLCAId: '2460f868-54a9-404c-bec8-c809a44ae72c'
                }
              }
            ],
            dataPrivate: false
          }
        ]
      ]);
    });
  });
});
