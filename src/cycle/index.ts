import {
  Cycle, CycleFunctionalUnit, SchemaType, Source, Completeness, Site, CycleDefaultMethodClassification
} from '@hestia-earth/schema';
import { reduceUndefinedValues } from '@hestia-earth/utils';

import { entitiesByType } from '../convert-interfaces';
import { diffInDays, findById, linkedNode } from '../utils';
import { Process, ProcessDocumentation, Source as OLCASource, Actor as OLCAActor, Exchange, Location } from '../olca';
import convertActor from '../actor';
import convertSite from '../site';
import convertSource from '../source';

// TODO: infrastructureProcess should convert content to `Infrastructure`
const skipConverting = (process: Process) => process.infrastructureProcess || process.processDocumentation?.copyright;

const parseDate = (date?: string | Date) => date ? date.toString().substring(0, 10) : undefined;

const parseCycleDuration = (startDate: string | Date, endDate: string | Date) =>
  endDate && startDate ? diffInDays(parseDate(startDate), parseDate(endDate)) : 0;

const sourceId = (doc: ProcessDocumentation) => doc.publication ?
  doc.publication['@id'] :
  (doc.sources?.length ? doc.sources[0]['@id'] : null);

const parseDefaultSource = (doc: ProcessDocumentation, entities: entitiesByType = {}) => {
  const id = sourceId(doc);
  const source = findById<OLCASource>(entities.Source, id);
  const authors = [
    doc.dataDocumentor ? findById<OLCAActor>(entities.Actor, doc.dataDocumentor['@id']) : null
  ].filter(Boolean);
  const sourceData = source ? convertSource(source) : { bibliography: {} } as Source;
  return reduceUndefinedValues<Source>({
    type: SchemaType.Source,
    id,
    ...sourceData,
    bibliography: {
      ...sourceData.bibliography,
      ...(authors.length ? {
        type: SchemaType.Bibliography,
        authors: authors.flatMap(convertActor)
      } : undefined)
    },
    intendedApplication: doc.intendedApplication
  });
};

const findLocation = (process: Process, entities: entitiesByType) =>
  (entities.Location || []).find(l => process.location && l['@id'] === process.location['@id']) as Location;

const parseSite = (process: Process, entities: entitiesByType, defaultSource?: Partial<Source>): Partial<Site> => {
  const location = findLocation(process, entities);
  const site = location ? convertSite(location).find(({ type }) => type === SchemaType.Site) : undefined;
  // TODO: override `siteType` from cycle.products.some(p => p.term.termType === 'crop') => 'cropland'
  return site ? reduceUndefinedValues({
    type: SchemaType.Site,
    ...site,
    defaultSource: linkedNode(defaultSource)
  }) : undefined;
};

// TODO: improve parsing
const parseDataCompleteness = ({ completenessDescription }: ProcessDocumentation): Completeness => {
  const defaultValue = (completenessDescription || '').indexOf('all intermediate flows are accounted') !== -1;
  return {
    type: SchemaType.Completeness,
    fertiliser: defaultValue,
    soilAmendments: defaultValue,
    excreta: defaultValue,
    water: defaultValue,
    electricityFuel: defaultValue,
    cropResidue: defaultValue,
    pesticidesAntibiotics: defaultValue,
    products: defaultValue,
    material: defaultValue,
    other: defaultValue,
    transport: defaultValue,
    waste: defaultValue,
    liveAnimals: defaultValue,
    animalFeed: defaultValue,
    operations: defaultValue
  };
};

const parseFunctionalUnit = (exchanges: Exchange[]) => {
  const reference = exchanges.find(e => e.quantitativeReference);
  return {
    functionalUnitDetails: reference ? `${reference.amount} ${reference.unit.name}` : undefined,
    functionalUnit: reference.unit.name === 'ha' && reference.amount === 1 ?
      CycleFunctionalUnit['1 ha'] : CycleFunctionalUnit.relative
  };
};

const exchangeType = ({ flow: { flowType }, input }: Exchange) =>
  input ? SchemaType.Input : (
    flowType === 'PRODUCT_FLOW' ? SchemaType.Product : SchemaType.Emission
  );

const parseExchange = (exchange: Exchange) => {
  const value = exchange.amount;
  const term = {
    type: SchemaType.Term,
    name: exchange.flow?.name,
    openLCAId: exchange.flow['@id']
  };
  const type = exchangeType(exchange);
  return {
    type,
    value: [value],
    term
  };
};

const filterExchanges = (exchanges, type: SchemaType) => exchanges.filter(v => v.type === type);

export default (process: Process, entities: entitiesByType = {}) => {
  const defaultSource = process.processDocumentation ?
    parseDefaultSource(process.processDocumentation, entities) :
    {
      type: SchemaType.Source,
      id: process['@id']
    } as Source;
  const site = parseSite(process, entities, defaultSource);
  const exchanges = (process.exchanges || []).map(parseExchange);
  return skipConverting(process) ? [] : [
    defaultSource,
    site,
    reduceUndefinedValues({
      type: SchemaType.Cycle,
      id: process['@id'],
      name: process.name,
      description: process.description,
      site: linkedNode(site),
      defaultMethodClassification: CycleDefaultMethodClassification.modelled,
      defaultMethodClassificationDescription: 'default',
      ...(process.processDocumentation ? reduceUndefinedValues({
        defaultSource: linkedNode(defaultSource),
        startDate: parseDate(process.processDocumentation.validFrom),
        endDate: parseDate(process.processDocumentation.validUntil),
        cycleDuration: parseCycleDuration(
          process.processDocumentation.validFrom,
          process.processDocumentation.validUntil
        ),
        completeness: parseDataCompleteness(process.processDocumentation)
      }) : {}),
      ...parseFunctionalUnit(process.exchanges),
      inputs: filterExchanges(exchanges, SchemaType.Input),
      products: filterExchanges(exchanges, SchemaType.Product),
      emissions: filterExchanges(exchanges, SchemaType.Emission),
      dataPrivate: false
    }) as Cycle
  ].filter(Boolean);
};
