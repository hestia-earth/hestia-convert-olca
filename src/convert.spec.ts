import { expect } from 'chai';
import * as sinon from 'sinon';
import 'mocha';
import { SchemaType } from '@hestia-earth/schema';

import * as jszip from 'jszip';
import { OLCAType } from './olca';
import { typeToFolder, convertSingle, convertZip } from './convert';
import * as specs from './convert';

let stubs: sinon.SinonStub[] = [];

describe('convert', () => {
  beforeEach(() => {
    stubs = [];
  });

  afterEach(() => {
    stubs.forEach(stub => stub.restore());
  });

  describe('typeToFolder', () => {
    it('should handle all types', () => {
      expect(typeToFolder(OLCAType.Actor)).to.equal('actors');
      expect(typeToFolder(OLCAType.Category)).to.equal('categories');
      expect(typeToFolder(OLCAType.FlowProperty)).to.equal('flow_properties');
      expect(typeToFolder(OLCAType.FlowPropertyFactor)).to.equal('flow_property_factors');
    });
  });

  describe('convertSingle', () => {
    const node: any = {};

    describe('no type', () => {
      beforeEach(() => {
        node['@type'] = undefined;
      });

      it('should return null', () => {
        expect(convertSingle({})(node)).to.equal(null);
      });
    });

    describe('type not in conversion types', () => {
      beforeEach(() => {
        node['@type'] = 'Random Type';
      });

      it('should return null', () => {
        expect(convertSingle({})(node)).to.equal(null);
      });
    });

    describe('type in conversion types', () => {
      beforeEach(() => {
        node['@type'] = OLCAType.Actor;
      });

      it('should return the node', () => {
        expect(convertSingle({})(node)).to.deep.equal([{
          type: SchemaType.Actor,
          dataPrivate: false
        }]);
      });
    });
  });

  describe('convertZip', () => {
    const files = {
      'actors/actor.json': {}
    };
    const actor = {
      '@type': 'Actor',
      '@id': 'actor-id',
      name: 'actor'
    };
    const zip: any = {
      files,
      file() {
        return {
          async: () => Promise.resolve(JSON.stringify(actor))
        };
      }
    };

    beforeEach(() => {
      stubs.push(sinon.stub(specs, 'convertSingle').returns(v => [v] as any));
      stubs.push(sinon.stub(jszip, 'loadAsync').returns(Promise.resolve(zip)));
    });

    it('should return the converted nodes', async () => {
      const { nodes } = await convertZip({}).toPromise();
      expect(nodes).to.deep.equal([actor]);
    });
  });
});
