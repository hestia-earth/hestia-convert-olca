import { JSZipObject, loadAsync } from 'jszip';
import * as pluralize from 'pluralize';
import { from } from 'rxjs';
import { map, mergeMap, filter, reduce, concatAll, distinct, toArray } from 'rxjs/operators';
import { JSON as HestiaJson, SchemaType, jsonldPath } from '@hestia-earth/schema';

import { sortNodes } from './utils';
import { OLCAType, Entity } from './olca';
import { entitiesByType, convertFunction } from './convert-interfaces';
import convertActor from './actor';
import convertCycle from './cycle';
import convertImpactAssessment from './impactAssessment';
import convertSite from './site';
import convertSource from './source';

export const typeToFolder = (type: OLCAType) => pluralize(type).replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();

const typeByFolder = Object.values(OLCAType)
  .reduce((prev, curr) => ({ ...prev, [typeToFolder(curr)]: curr }), {} as { [folder: string]: OLCAType });

const convertSingleByType: {
  [type in OLCAType]?: convertFunction;
} = {
  [OLCAType.Actor]: convertActor,
  [OLCAType.Source]: convertSource,
  [OLCAType.Process]: convertCycle,
  [OLCAType.ProductSystem]: convertImpactAssessment,
  [OLCAType.Location]: convertSite
};

export const convertSingle = (entities: entitiesByType = {}) => (node: Entity): HestiaJson<SchemaType>[] =>
  '@type' in node && node['@type'] in convertSingleByType ? convertSingleByType[node['@type']](node, entities) : null;

interface IFileExtract {
  id: string;
  type: OLCAType;
  content: Entity;
}

const fileDetails = (path: string) => {
  const [folder, filename] = path.split('/');
  const type = typeByFolder[folder];
  return type && filename ? { path, type } : null;
};

const extractFile = async (path: string, file: JSZipObject) => {
  const [folder, filename] = path.split('/');
  const type = typeByFolder[folder];
  const content = JSON.parse(await file.async('string'));
  return { id: filename.split('.')[0], type, content } as IFileExtract;
};

const defaultProperties = [
  'type',
  'id',
  'dataPrivate'
];

const isExportable = (node: any) =>
  Object.keys(node).some(key => !defaultProperties.includes(key));

export const convertZip = (data: any) =>
  from(loadAsync(data)).pipe(
    mergeMap(zip => from(Object.keys(zip.files)).pipe(
      map(fileDetails),
      filter(Boolean),
      mergeMap(({ path }) => extractFile(path, zip.file(path)))
    )),
    reduce((prev, curr) => {
      prev[curr.type] = prev[curr.type] || [];
      prev[curr.type].push(curr.content);
      return prev;
    }, {} as entitiesByType),
    mergeMap(nodes => from(Object.keys(nodes)).pipe(
      map(type => nodes[type].flatMap(convertSingle(nodes))),
      concatAll(),
      filter(node => !!node && isExportable(node)),
      distinct((value: HestiaJson<SchemaType>) => jsonldPath(value.type, value.id)),
      toArray()
    )),
    map(nodes => ({ nodes: sortNodes(nodes) }))
  );
