import { expect } from 'chai';
import 'mocha';
import { SchemaType } from '@hestia-earth/schema';

import { readFileAsJSON } from '../../test/utils';

import convert from './';

describe('bibliography > index', () => {
  describe('convert', () => {
    it('should convert to hestia format', () => {
      expect([
        convert(readFileAsJSON('source/6e6e4ce8-fc02-30c8-bd5d-68ada1b5cc9d.json')),
        convert(readFileAsJSON('source/9091935f-4c62-88a7-3404-b3de080fb7c1.json'))
      ]).to.deep.equal([
        {
          type: SchemaType.Source,
          id: '6e6e4ce8-fc02-30c8-bd5d-68ada1b5cc9d',
          name: 'Cooper, J.S. (2015)',
          bibliography: {
            type: SchemaType.Bibliography,
            title: 'Summary of Revisions of the LCA Digital Commons Unit Process Data' +
              ': field crop production, Version 2 2015',
            name: 'Cooper, J.S. (2015)',
            year: 2015,
            documentDOI: '10.15482/USDA.ADC/1255530'
          }
        },
        {
          type: SchemaType.Source,
          id: '9091935f-4c62-88a7-3404-b3de080fb7c1',
          name: 'Cooper, J.S., Kahn, E. (2012)',
          bibliography: {
            type: SchemaType.Bibliography,
            title: 'Commentary on issues in data quality analysis in life cycle assessment',
            name: 'Cooper, J.S., Kahn, E. (2012)',
            year: 2012
          }
        }
      ]);
    });
  });
});
