import { Source, SchemaType } from '@hestia-earth/schema';
import { reduceUndefinedValues } from '@hestia-earth/utils';

import { Source as OLCASource } from '../olca';

const formatName = (name: string, year?: number) =>
  name ? `${name.replace('(', '').trim()}${year ? ` (${year})` : ''}` : undefined;

const parseReference = (reference: string, originalYear?: number) => {
  const [_, name, parsedYear] = reference ? reference.match(/([^\d]*)(\d+)(.*)/) : [
    undefined, undefined, undefined
  ];
  const year = originalYear || (parsedYear ? +parsedYear : undefined);
  return {
    name: formatName(name, year),
    year
  };
};

const parseDocumentDOI = (url = '') => url.indexOf('dx.doi.org') !== -1 ? url.replace('dx.doi.org/', '') : undefined;

export default (source: OLCASource): Source => {
  const { name, year } = parseReference(source.textReference, source.year);
  const documentDOI = parseDocumentDOI(source.url);
  const websites = [
    documentDOI ? undefined : source.url,
    source.externalFile
  ].filter(Boolean);
  return reduceUndefinedValues({
    type: SchemaType.Source,
    id: source['@id'],
    name,
    bibliography: reduceUndefinedValues({
      type: SchemaType.Bibliography,
      title: source.name,
      name,
      year,
      documentDOI,
      websites
    })
  }) as Source;
};
