import { expect } from 'chai';
import 'mocha';
import { readFile } from 'fs';
import { join } from 'path';

import { FIXTURES_FOLDER, readFileAsJSON } from '../test/utils';

import { convertZip } from './index';

describe('integration', () => {
  describe('convertZip', () => {
    it('should convert the sample zip to Hestia', async () => {
      const expected = readFileAsJSON('hestia/sample-converted.json');
      const zip = await new Promise((resolve, reject) =>
        readFile(join(FIXTURES_FOLDER, 'sample.zip'), (err, data) => err ? reject(err) : resolve(data))
      );
      const result = await convertZip(zip).toPromise();
      // console.log(JSON.stringify(result, null, 2));
      expect(result).to.deep.equal(expected);
    });

    it('should convert the coconuts zip to Hestia', async () => {
      const expected = readFileAsJSON('hestia/coconuts-converted.json');
      const zip = await new Promise((resolve, reject) =>
        readFile(join(FIXTURES_FOLDER, 'coconuts.zip'), (err, data) => err ? reject(err) : resolve(data))
      );
      const result = await convertZip(zip).toPromise();
      // console.log(JSON.stringify(result, null, 2));
      expect(result).to.deep.equal(expected);
    });
  });
});
