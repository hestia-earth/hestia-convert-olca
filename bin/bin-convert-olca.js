#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var index = require("../dist/index");

var start = new Date();
var args = process.argv.slice(2);
var src = args[0];
if (!src) {
  throw new Error('No source file given. Usage: hestia-convert-olca <src> <dest>');
}
var dest = args[1];
if (!dest) {
  throw new Error('No dest file given. Usage: hestia-convert-olca <src> <dest>');
}

var data = fs.readFileSync(src);

function writeToFile(data) {
  fs.writeFileSync(dest, JSON.stringify(data, null, 2), 'utf-8');
  console.log('Done in ' + (new Date().getTime() - start.getTime()) + 'ms');
  process.exit(0);
}

try {
  data = JSON.parse(data);
  writeToFile({ nodes: [index.convertSingle({})(data)] });
}
catch (err) {
  if (!(err instanceof SyntaxError)) {
    console.error(err);
  }
  index.convertZip(data).toPromise().then(writeToFile).catch(function (err) {
    console.error(err);
    process.exit(1);
  });
}
