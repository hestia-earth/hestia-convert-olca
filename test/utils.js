const { readFileSync } = require('fs');
const { join } = require('path');

// default values
const FIXTURES_FOLDER = join(__dirname, 'fixtures');
const ENCODING = 'utf8';

const readFileAsString = (file, folder = FIXTURES_FOLDER) => readFileSync(join(folder, file), ENCODING);
const readFileAsJSON = (...args) => JSON.parse(readFileAsString(...args));

module.exports = {
  FIXTURES_FOLDER,
  readFileAsString,
  readFileAsJSON
};
