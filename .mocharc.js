'use strict'

module.exports = {
  require: [
    'ts-node/register',
    'source-map-support/register'
  ],
  'full-trace': true,
  'watch-files': 'src',
  timeout: 30000
}
