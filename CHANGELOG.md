# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.5.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.4.0...v0.5.0) (2023-07-31)


### ⚠ BREAKING CHANGES

* **package:** Requires schema version `22`.

* **package:** use schema version `22` ([a1b2975](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/a1b29759b3e7bcd55539a669c81d748b2cf5c6c4))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.3.0...v0.4.0) (2023-04-03)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `18`

* **package:** update schema to `18` ([15cfacb](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/15cfacbd9bf0158b7dc6b50f5d9df5bc2d202fb5))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.2.0...v0.3.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* **cycle:** min schema version is `14`

### Features

* **cycle:** add required fields from version 14 ([f1fbcd5](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/f1fbcd53eaf88826d445175179369e5b1967e04c))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.1.1...v0.2.0) (2022-09-09)


### ⚠ BREAKING CHANGES

* **package:** min schema supported version is `11.0.0`

* **package:** update schema to `11.0.0` ([db84e83](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/db84e83cbef3fdc1c0962bb5516cfa3a6be25cfc))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.1.0...v0.1.1) (2021-10-01)

## [0.1.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.5...v0.1.0) (2021-10-01)


### ⚠ BREAKING CHANGES

* use schema min `5.3.0`

### Features

* use schema `5.3.0` ([ce3d892](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/ce3d892b424c7b5da5686589cde68b85be98cd9f))


### Bug Fixes

* **impact assessment:** remove `functionalUnitMeasure` ([c84e157](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/c84e15797b64ad6661222d1d062e8168652c2732))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.4...v0.0.5) (2021-05-21)


### Features

* **cycle:** handle inputs/products/emissions ([e262d18](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/e262d1855f2a194e603e6164d0ba8e1e2e9cb669))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.3...v0.0.4) (2021-05-20)


### Features

* **package:** require glossary `0.0.6` ([26f38d4](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/26f38d4d2199e909c1967cf410d01a254b7264f6))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.2...v0.0.3) (2021-05-20)

### [0.0.2](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.1...v0.0.2) (2021-05-20)

### 0.0.1 (2021-05-20)


### Features

* update schema to 0.1.2 and fix errors ([ca5fa92](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/ca5fa92ed610a8c45509ba812c025dd31442ecdd))
* **bibliography:** convert from Source ([7d8afcb](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/7d8afcb408c2f5261004eb74ed1c8f57efcec927)), closes [#7](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/7)
* **convert:** add first conversion for Actor ([c3f9bcd](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/c3f9bcdc34ec337f8586723bbefe876bbe94989e))
* **convert:** convert zip file ([7c16204](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/7c1620411812db5b08ab80a1136a4fbc166ac2c1))
* **cycle:** add simple parsing of dataCompleteness ([85d573f](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/85d573f8d46010b4d09181f542efcdcc63efca34)), closes [#1](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/1)
* **cycle:** calculate duration from endDate - startDate ([a76bd57](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/a76bd570734a249a2b4c8b86d434db123a7f4fe8)), closes [#1](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/1)
* **cycle:** convert exchanges as inputs ([251ec7b](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/251ec7bd32ceb1b1b852a7ac6cb4f31017506860))
* **cycle:** convert from Process ([ba1f6df](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/ba1f6df0ae29b4bd732d79a2bd8adf593c4f10ad)), closes [#1](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/1)
* **cycle:** handle functionalUnitMeasure and functionalUnitDetails ([fbd4741](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/fbd47416ac54ed8d6f1a6d7df7b214a738f1d69c)), closes [#1](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/1)
* **cycle:** map `authors` for `Source` ([32d73ce](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/32d73ce639daa541379b97bfa9cc6d6b894bcb4b)), closes [#7](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/7)
* **cycle:** parse startDate / endDate from processDocumentation ([8bb7362](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/8bb7362d263ef8278b516519a016531560deb233)), closes [#1](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/1)
* **cycle:** set default functionalUnitMeasure to "relative" ([ca2335f](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/ca2335fbeace0326779d3075d40fbee9ab23f282)), closes [#1](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/1)
* **cycle:** skip parsing if copyright ([f0cf835](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/f0cf835db44192d135e89c355c5c49fc7af5badc))
* **inventory:** add basic conversion ([fe2bf64](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/fe2bf64bb38412ae9790a32e4b176f5da9ebca20)), closes [#5](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/5)
* **inventory:** parse cycle from referenceProcess ([6910952](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/691095251a023db94f8b4e1a6b298d567f1da187)), closes [#5](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/5)
* **olca:** add generated types ([5d9dd9d](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/5d9dd9d680477151fdf24a33e69c7d27e287fcdb))
* **site:** add mapping iso code to country name ([aabc3e5](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/aabc3e5570534b93188bc3406e281a748d29f1c6))
* **site:** convert from Location ([d67184c](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/d67184c5844736921c49a4fb25bba9d53c2dfe81)), closes [#3](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/3)
* **site:** handle both country and region ([f4b20a2](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/f4b20a2067c5a5e3bc52426d026c9742ca64e1af))
* **site:** set default `siteType` to `not specified` ([da0ecc2](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/da0ecc286759a3615fd974ea7f76eaa603b63ca4)), closes [#3](https://gitlab.com/hestia-earth/hestia-convert-olca/issues/3)
* **source:** set bibliography.name ([c287525](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/c28752535de67c8a9840913a9f6874a865b5ce5f))
* add bin script to convert files ([4444044](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/444404480362491684360071458c52aaa69ad99a))


### Bug Fixes

* **actor:** parse firstName and lastName ([4870823](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/4870823f1b61badeb50697c15b1fe73939d5d5f0))
* **bibliography:** append year in name ([af1956e](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/af1956e9bc15a94f828b8cf96bbfd03e205ea4c1))
* **bibliography:** skip website if url is DOI ([5af238a](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/5af238ae3d8d05a967f7dd8f43fedd35965c5713))
* **cycle:** handle incomplete process documentation ([cac21d6](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/cac21d64ceeb81580bed1c2ceff853d6c5e8deda))
* **olca:** set extends using superClass ([501f00c](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/501f00c9eccffe817d7e5526de3b9ada677557d0))
* **site:** skip latitude/longitude when both are 0 ([ff3ddef](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/ff3ddeffefa939e63ee2b480ffdd8cc11d13f17f))
