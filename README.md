# Hestia Convert openLCA

[![Pipeline Status](https://gitlab.com/hestia-earth/hestia-convert-olca/badges/master/pipeline.svg)](https://gitlab.com/hestia-earth/hestia-convert-olca/commits/master)
[![Coverage Report](https://gitlab.com/hestia-earth/hestia-convert-olca/badges/master/coverage.svg)](https://gitlab.com/hestia-earth/hestia-convert-olca/commits/master)

Hestia Library to convert openLCA format to Hestia format

## Install

```sh
npm install @hestia-earth/olca --save
```

## Usage

You can use the bin file directly to convert a json or ZIP file from your command line:
```bash
hestia-convert-olca <src file> <dest file>
```
The content will be saved in the dest file. You can them import the file on the Hestia platform!

### Using as a library

You can also integrate the library in your existing NodeJS project:
```typescript
import { readFile } from 'fs';
import { convertZip } from '@hestia-earth/olca';

const data = await new Promise((resolve, reject) =>
  readFile(join(FIXTURES_FOLDER, 'sample.zip'), (err, data) => err ? reject(err) : resolve(data))
);
const result = await convertZip(zip);
```
