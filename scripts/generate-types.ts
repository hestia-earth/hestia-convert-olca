import { readdirSync, readFileSync, mkdirSync, writeFileSync } from 'fs';
import { join } from 'path';
import { parse } from 'yamljs';

const ensureDir = (dir: string) => {
  try { mkdirSync(dir, { recursive: true }) } catch (err) { }
};

const readYmlFile = (file: string) => parse(readFileSync(join(YAML_DIR, file), 'utf8'));

const listYmlDir = () => readdirSync(YAML_DIR);

const YAML_DIR = join(__dirname, '..', 'olca');
const CLASS_DIR = join(__dirname, '..', 'src', 'olca');
const MAX_LENGTH = 120;

ensureDir(CLASS_DIR);

enum PropertyType {
  array = 'array',
  double = 'double',
  number = 'number',
  int = 'int',
  integer = 'integer',
  string = 'string',
  boolean = 'boolean',
  date = 'date',
  dateTime = 'dateTime',
  GeoJSON = 'GeoJSON'
}

const typeToClass: {
  [type in PropertyType]: string;
} = {
  [PropertyType.array]: 'any[]',
  [PropertyType.double]: 'number',
  [PropertyType.number]: 'number',
  [PropertyType.int]: 'number',
  [PropertyType.integer]: 'number',
  [PropertyType.string]: 'string',
  [PropertyType.boolean]: 'boolean',
  [PropertyType.date]: 'Date',
  [PropertyType.dateTime]: 'Date',
  [PropertyType.GeoJSON]: 'any'
};

interface IProperty {
  name: string;
  type?: PropertyType;
  doc?: string;
  enum?: string[];
};

interface IClass {
  name: string;
  superClass?: string;
  examples?: string[];
  doc?: string;
  properties?: IProperty[];
}

interface IItem {
  name: string;
  doc?: string;
}

interface IEnum {
  name: string;
  doc?: string;
  items: IItem[];
}

const findPropertyType = (type: string) => type in typeToClass ? typeToClass[type] : type;

const cleanType = (type: string) => type
  .replace(/(Embed\[)([a-zA-Z]*)(\])/g, '$2')
  .replace(/(Ref\[)([a-zA-Z]*)(\])/g, '$2')
  .replace(/(List\[)([a-zA-Z]*)(\])/g, '$2[]')
  .replace(/(array\[)([a-zA-Z]*)(\])/g, '$2[]');

const dependencies = ({ name, properties }: IClass) => (properties || [])
  .map(({ type }) => findPropertyType(type))
  .filter(type =>
    !Object.values(typeToClass).includes(type) && (
      type.includes('Embed') || type.includes('Ref') || type.includes('List') ||
      type.match(/[A-Z]+/) !== null
    )
  )
  .map(type => cleanType(type).replace(/\[\]/g, ''))
  .filter(type => !Object.keys(typeToClass).includes(type) && type !== name);

const cleanPropertyName = (name: string) =>
  name.includes('-') || name.includes(' ') ? `'${name}'` : name;

const propertyIsEnum = (property: Partial<IProperty>) =>
  !!property.enum && property.enum.every(val => typeof val === 'string');

const propertyEnumName = (className: string, { name }: Partial<IProperty>) =>
  `${className}${name.charAt(0).toUpperCase()}${name.substring(1)}`;

const propertyEnum = (className: string, property: Partial<IProperty>) => propertyIsEnum(property) ?
  propertyEnumName(className, property) :
  property.enum.map(t => property.type === PropertyType.string ? `'${t}'` : t).join('|');

const formatDoc = (doc: string) => `${doc ? `/**
* ${doc.trim().match(new RegExp(`.{1,${MAX_LENGTH - 5}}`, 'g')).join('\n* ')}
*/` : ''}`;

const generateProperty = (className: string) => ({ name, type, doc, enum: types }: IProperty) =>
  `${formatDoc(doc)}
  ${cleanPropertyName(name)}?: ${types ? propertyEnum(className, { name, type, enum: types }) : cleanType(findPropertyType(type))};
`;

const JSONContent = ({ className, parentClass, doc, properties }) => `
${formatDoc(doc)}
export class ${className} ${parentClass ? `extends ${parentClass}` : ''} {
  ${properties.map(generateProperty(className)).join('\n  ')}
}`;

const generateEnums = (className: string, properties: IProperty[]) =>
properties.filter(propertyIsEnum).map(property => `
export enum ${propertyEnumName(className, property)} {
  ${property.enum.sort().map(val => `${cleanPropertyName(val)} = '${val}'`).join(',\n  ')}
}`).join('\n\n');

const generateEnum = ({ name, items }: IEnum) => {
  const content = `// auto-generated content

export enum ${name} {
  ${(items || []).sort().map(({ name }) => `${cleanPropertyName(name)} = '${name}'`).join(',\n  ')}
}`;
  writeFileSync(join(CLASS_DIR, `${name}.ts`), content);
  return { name };
};

const generateEntity = ({ name }: IClass) => {
  const content = `// auto-generated content
import { OLCAType } from './types';

export interface IContext {
  '@base': string;
  '@vocab': string;
}

export class ${name} {
  '@context': string|Array<string|IContext>;
  '@type': OLCAType;
  '@id': string;
}`;
  writeFileSync(join(CLASS_DIR, `${name}.ts`), content);
  return { name };
};

const generateClass = (file: string) => {
  console.log(`Processing file: ${file}`);
  const data = readYmlFile(file);
  if (data.enum) {
    return generateEnum(data.enum);
  }
  if (data.class.name === 'Entity') {
    return generateEntity(data.class);
  }
  const properties: IProperty[] = data.class.properties || [];
  const importedClasses = [...dependencies(data.class), data.class.superClass].filter(Boolean);
  const content = `// auto-generated content
${[...new Set(importedClasses)].map(name => `import { ${name} } from './${name}';`).join('\n')}
${generateEnums(data.class.name, properties)}
${JSONContent({ className: data.class.name, parentClass: data.class.superClass, doc: data.class.doc, properties })}`;
  writeFileSync(join(CLASS_DIR, `${data.class.name}.ts`), content);
  return {
    name: data.class.name
  };
};

const generateTypes = (classes: { name: string }[]) => {
  const content = `// auto-generated content

export enum OLCAType {
  ${classes.map(({ name }) => `${name} = '${name}'`).join(',\n  ')}
}
`;
  writeFileSync(join(CLASS_DIR, 'types.ts'), content);
};

const generateIndex = (classes: string[]) => {
  const content = `// auto-generated content

export * from './types';

${classes.map(name => `export * from './${name}';`).join('\n')}
`;
  writeFileSync(join(CLASS_DIR, 'index.ts'), content);
};

const run = () => {
  const files = listYmlDir();
  const classes = files.map(generateClass).filter(Boolean);
  generateTypes(classes);
  generateIndex(classes.map(({ name }) => name));
};

run();
