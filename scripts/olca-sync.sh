#! /bin/bash

TMP_DIR="olca-tmp"

mkdir -p $TMP_DIR
cd $TMP_DIR
rm -rf *
curl -L https://api.github.com/repos/GreenDelta/olca-schema/tarball > repo.tar.gz
tar xvzf repo.tar.gz
mv */yaml ../olca
cd ../
rm -rf $TMP_DIR
